#### A /eɪ/
> + **A is for Attitude**./ˈætɪtjuːd/
> + **A is for Acknowledge.**/əkˈnɒlɪdʒ/
> + **At here waiting** /ˈweɪtɪŋ/ **for you.**
> + **Always** /ˈɔːlweɪz/ **waiting for you.** 永远等着你.

#### B /biː/
> + **B is for Belief.**/bɪˈliːf/
> + **You are what you believe!**/bɪˈliːv/
> + **Be with you.** 和你在一起.

#### C /siː/
> + **C is for Confidence.**/ˈkɒnfɪdəns/
> + **Who has confidence in himself will gain** /ɡeɪn/ **confidence of others.**
> + **Calls you just to say Hi.**

#### D /diː/
> + **D is for Dreams.**/driːm/
> + **let's keep our dreams alive!**/əˈlaɪv/
> + **Doesn't mind** /maɪnd/ **you shout** /ʃaʊt/ **at me.**
> + **Dear, Good night.** 每晚温柔地对你说晚安.

#### E /iː/
> + **E is for Easy.**/ˈiːzi/
> + **Take it easy and enjoy** /ɪnˈdʒɔɪ/ **your life!**
> + **Envisions** /ɪnˈvɪʒn/ **the whole** /həʊl/ **of you.**
> + **Expect** /ɪkˈspekt/ **the whole of you.** 期待你的全部

#### F /ef/
> + **F is for Fun.**/fʌn/
> + **Go out and have some fun!**
> + **Forever** /fərˈevə(r)/ **stand** /stænd/ **by you.** 永远在你身边

#### G /dʒiː/
> + **G is for Giving.**/ɡɪv/
> + **It is in giving that we receive.**/rɪˈsiːv/有付出才有收获.
> + **Gives you what you needed.** 给你一切你所需的

#### H /eɪtʃ/
> + **H is for Happiness.**/ˈhæpinəs/
> + **Happiness is nothing more than good health** /helθ/ **and a bad memory.**
> + **Hope you enjoy your life.**

#### I /aɪ/
> + **I is for Innovation.**/ˌɪnəˈveɪʃn/
> + **Innovation is the ability** /əˈbɪləti/ **to see change** /tʃeɪndʒ/ **as an opportunity.**/ˌɒpəˈtjuːnəti/

#### J /dʒeɪ/
> + **J is for Joy.**/dʒɔɪ/
> + **A thing of beauty** /ˈbjuːti/ **is a joy for ever.**/ˈevə(r)/美丽的事物是永久的享受.
> + **You jump,I jump!** 如影随形.

#### K /keɪ/
> + **K is for kindness.**/ˈkaɪndnəs/
> + **Kindness is the language** /ˈlæŋɡwɪdʒ/ **which the deaf** /def/ **can hear and the blind** blind **can see.**
> + **Keeps you close at heart.** 靠近你的内心.
> + **Kiss you when you wake.** 在你醒来时偷偷吻你.

#### L /el/
> + **L is for Love.**/lʌv/
> + **Love is a sweet** /swiːt/ **torment.**/ˈtɔːment/
> + **Let's say 'thank you' to our family and friends!**
> + **Learn to know you.** 学会懂你.

#### M /em/
> + **M is for Memory.**/ˈmeməri/
> + **Every man's memory is his private** /ˈpraɪvət/ **literature.**/ˈlɪtrətʃə(r)/
> + **Makes a difference** /ˈdɪfrəns/ **in your life.**
> + **Make more surprise** /səˈpraɪz/ **in your life.** 给你与众不同的生活.

#### N /en/
> + **N is for Nostalgia.**/nɒˈstældʒə/
> + **Nostalgia is like a grammar** /ˈɡræmə(r)/ **lesson** /ˈlesn/ **: you find the present** /ˈpreznt/ **tense** /tens/**, but the past** /pɑːst/ **perfect!**/ˈpɜːfɪkt/
> + **Never** /ˈnevə(r)/ **make you cry.** 永远不让你哭泣.

#### O /əʊ/
> + **O is for Outlook.**/ˈaʊtlʊk/
> + **We need to have a tolerant** /ˈtɒlərənt/ **outlook on life.**
> + **Offer** /ˈɒfə(r)/ **support.**/səˈpɔːt/我支持你.

#### P /piː/
> + **P is for Patience.**/ˈpeɪʃns/
> + **Patience is a virtue.**/ˈvɜːtʃuː/
> + **Put you in my heart.** 把你放在我心底.

#### Q /kjuː/
> + **Q is for Question.**/ˈkwestʃən/
> + **He who questions nothing learns nothing.**
> + **Quit** /kwɪt/ **your fears.**/fɪə(r)/停止你的害怕.

#### R /ɑː(r)/
> + **R is for Regret.**/rɪˈɡret/
> + **Forget regret, or life is yours to miss.**
> + **Run with you.** 和你一起去未来.

#### S /es/
> + **S is for Sharing.**/ˈʃeərɪŋ/
> + **There is no delight** /dɪˈlaɪt/ **in owning anything unshared.**
> + **Smile** /smaɪl/ **when it hurts most.**
> + **Sing a song for you.** 为你唱一首属于你的歌.

#### T /tiː/
> + **T is for Teamwork.**/ˈtiːmwɜːk/
> + **Teamwork divides** /dɪˈvaɪd/ **the task and doubles** /ˈdʌbl/ **the success.**
> + **Taciturn** /ˈtæsɪtɜːn **knight.**/naɪt/
> + **To be yours.** 我是你的.

#### U /juː/
> + **U is for Unity.**/ˈjuːnəti/ 
> + **United We Stand** /stænd/ **, Divided We Fall!**/fɔːl/
> + **Understant you.** 懂你.

#### V /viː/
> + **V is for Victory.**/ˈvɪktəri/
> + **Victory is sweetest when you've known defeat.**/dɪˈfiːt/
> + **Values you.** 珍爱你.
> + **Values myself on you.** 以你为荣.

#### W /ˈdʌbljuː/
> + **W is for Wealth.**/welθ/
> + **Ability** /əˈbɪləti/ **is a poor man's wealth.**
> + **Walks besides** /bɪˈsaɪdz/ **you.** 与你同行.
> + **Wake you up everyday.** 每天叫醒爱赖床的你.

#### X /eks/
> + **X is for X.**
> + **Curiosity** /ˌkjʊəriˈɒsəti/ **is the wick** /wɪk/ **in the candle** /ˈkændl/ **of learning.**
> + **XL love.** 特大号的爱.

#### Y /waɪ/
> + **Y is for Youth.**/juːθ/
> + **In youth we learn; in age we understand.**
> + **You're all the time so addictive.**/əˈdɪktɪv/你一直是如此地令我着迷.

#### Z /zed/
> + **Z is for Zeal.**/ziːl/
> + **Never** /ˈnevə(r)/ **lose** /luːz/ **your zeal.**
> + **Zeal for you.** 对你狂热